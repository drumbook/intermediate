\include "definitions.ly"
\version "2.20.0"
\header {
  title = "4"
  meter = "Andante"
}
\drums {
  \time 3/8
  \tempo 4. = 88
  %
  % intro
  sn8\< sn sn
  sn8 sn16 sn sn8\!
  r4 sn16\p sn
  sn16 sn sn sn sn8
  r4.
  r16 sn16\pp sn sn sn8
  r4.
  sn16 sn sn8 r
  r4.\fermata
  %
  % part a
  sn8\f sn16 sn sn8
  sn8 sn16 sn sn sn
  sn8 sn16 sn sn8
  r8 sn\mf sn
  sn8 sn16 sn sn8
  sn8[ r sn ]
  sn8 sn16 sn sn8
  sn8[ r sn ]
  %
  sn8. sn16 sn8
  sn8. sn16 sn8
  sn16 sn sn sn sn8
  r16 sn sn sn sn8
  r4 sn8
  sn8. sn16 sn8
  sn8. sn16 sn8
  r16 sn sn8 r
  %
  sn8. sn16 sn8
  sn8 sn16 sn sn8
  sn8. sn16 sn8
  sn8 sn16 sn sn8
  r8 sn\< sn
  sn8 sn16 sn sn8\f
  r8 r8. sn16
  sn8 sn16 sn sn8
  %
  % part b
  r8 sn16\p sn sn8
  sn16 sn sn sn sn8
  r16 sn sn sn sn8
  r16 sn sn8 r
  sn8\mf sn sn16 sn
  sn8 sn r
  r8 \tuplet 3/2 { sn16 sn sn } r8
  sn8 sn16 sn sn8
  %
  r16 sn sn sn sn8
  sn8 \tuplet 3/2 { sn16 sn sn } r8
  r8 sn16 sn sn8
  r8 \tuplet 3/2 { sn16 sn sn } sn8
  r8 sn8. sn16
  sn8. sn16 sn8
  sn8 sn16 sn sn8
  r4.
  % part c
  sn16\p sn sn sn sn sn
  sn8 sn16 sn sn sn
  sn16 sn sn8 r
  sn16 sn sn sn sn sn
  sn16 sn sn sn sn8
  r16 sn sn sn sn sn
  sn4.
  r16 sn sn sn sn sn
  sn16 sn sn8 r
  \tuplet 3/2 { sn16 sn sn } sn8 sn
  r8 \tuplet 3/2 { sn16 sn sn } sn8
  sn16 sn sn8 r
  %
  % part d
  r8 \tuplet 3/2 { sn16\mf sn sn } sn8
  r8 \tuplet 3/2 { sn16 sn sn } sn8
  sn16 sn sn8 r
  r8 \tuplet 3/2 { sn16 sn sn } sn8
  sn8. sn16 sn8
  sn16 sn sn sn sn8
  r8 sn16 sn sn sn
  sn16 sn sn8 r
  %
  sn16 sn sn sn r8
  sn8. sn16 sn8
  sn16 sn sn sn r8
  \drag sn4.
  sn8 \tuplet 3/2 { sn16 sn sn } sn8
  sn8. sn16 sn8
  \drag sn4.
  r8 sn\< sn
  %
  % part e
  sn8 sn16 sn sn8\f
  r8 sn16 sn sn sn
  sn8 sn16 sn sn8
  r8 sn sn
  sn8 sn16 sn sn8
  sn8[ r sn ]
  sn8[ r sn ]
  sn8 sn16 sn sn8
  sn8 sn16 sn sn8
  \time 2/4
  \tempo \markup {
    \concat {
      \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
    }
  }
  sn16\decresc sn sn8 sn16 sn sn8
  sn16 sn sn8 sn16 sn sn8
  sn16 sn sn8 sn16 sn sn8
  sn16 sn sn8 \drag sn4\!
  \time 3/8
  \tempo \markup {
    \concat {
      \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
    }
  }
  %
  % coda
  sn8\< sn sn
  sn8 sn16 sn sn8\!
  r4 sn16\p sn
  sn16 sn sn sn sn8
  r4.
  r16 sn16\pp sn sn sn8
  r4.
  sn16 sn sn8 r
  r4.
  sn4.:32 ~\fermata\decresc
  sn8\! r4 \bar "|."

}
