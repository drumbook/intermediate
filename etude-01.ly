\include "definitions.ly"
\version "2.20.0"
\header {
  title = "1"
  meter = "Allegretto"
}
\drums {
  \time 4/4
  \tempo 2 = 112
  % part a
  sn4^>\f sn8 sn sn4 sn^>
  sn8 sn r sn sn sn sn4
  sn8^> sn sn sn r sn sn sn
  sn4 sn8[ sn sn sn ] r sn
  \break
  sn4 sn8 sn sn4 sn^>\<\sfz
  r8 sn sn sn sn sn sn4^>\!
  sn8 sn sn4 sn sn8 sn
  r8 sn sn sn r sn\< sn sn
  sn8 sn sn4\! sn8 sn sn4
  \break
  sn8^> sn sn sn r sn sn sn
  sn8 sn sn4 sn8^> sn sn sn
  r8 sn sn sn sn sn r4
  r4 sn8[\p sn sn sn ] r sn
  \break
  sn8 sn sn4 sn8\f sn sn4
  sn8\p sn sn sn r sn r sn
  sn8 sn sn4 r8 sn sn sn
  \break

  % part b
  sn4 sn8^> sn sn sn r sn^>
  sn8 sn sn4 sn8^> sn sn sn
  r8 sn^> sn sn sn4 r8 sn\f
  sn8 sn sn sn sn sn r sn
  \break
  sn8 sn sn4 r8 sn sn sn
  sn8 sn r sn sn sn sn4
  sn8 sn sn sn r sn sn sn
  sn4 r8 sn sn4 r
  \break
  r4 r8 sn\< sn sn sn4^>\!
  sn8 sn sn4 sn8 sn sn sn
  r8 sn sn sn sn4 sn8 sn
  sn8 sn sn4 sn r
  \break

  % part c
  sn4^> sn8 sn sn4 sn^>
  sn8 sn sn4 sn8 sn sn4
  \time 6/4
  sn4^> sn8 sn sn4 sn^> sn8 sn sn4
  \break
  sn4^> sn8 sn sn sn r sn sn sn sn4
  sn4^> sn8 sn r sn r sn sn sn sn4
  sn8 sn sn sn r sn sn sn sn4 r
  \break
  \time 4/4
  sn4^> sn8 sn sn4 sn^>
  sn8 sn sn4 r r8 sn
  sn8 sn sn4 sn8^^ sn^^ sn4^^
  \bar "|."
  
}
