\include "definitions.ly"
\version "2.20.0"
\header {
  title = "3"
  meter = "Allegro"
}
\drums {
  \time 4/4
  \tempo 4 = 128
  %
  % part a
  sn16\f sn sn sn sn sn sn sn sn sn sn sn sn sn sn sn
  sn16\> sn sn sn sn sn sn sn sn sn sn sn sn sn sn sn
  sn16\!\< sn sn sn sn sn sn sn sn sn sn sn sn sn sn8\!
  sn16 sn sn sn sn sn sn sn r sn sn sn sn sn sn8
  %
  sn16 sn sn8 r16 sn sn sn sn sn sn8 r4
  sn16\mf sn sn sn sn sn sn sn sn sn sn sn sn sn sn8
  sn16 sn sn sn sn sn sn8 r16 sn sn sn sn sn sn8
  sn8 sn16 sn sn sn sn8 r16 sn sn sn sn8 sn16 sn
  %
  sn16 sn sn8 sn16 sn sn sn sn sn sn8 sn16 sn sn8
  sn8 sn16 sn sn8. sn16 sn sn sn8 r16 sn sn sn
  sn8. sn16 sn sn8 sn16 sn sn sn8 r4
  sn16 sn sn sn sn sn sn sn sn sn sn sn sn sn sn8
  %
  sn16 sn sn sn r sn sn sn sn8. sn16 sn sn sn8
  r16 sn sn sn sn sn sn sn r sn sn sn sn sn sn8
  sn8. sn 16 sn sn sn8 r16 sn sn8 r4
  %
  % part b
  sn4\f sn8. sn16 \flam sn4 r
  r8. sn16\p sn sn sn8 r16 sn sn sn sn4
  r4 r16 sn sn sn r8. sn16 sn sn8.
  r8. sn16 sn sn8. r16 sn sn sn r4
  \break
  sn16 sn sn8 r8 sn16 sn sn sn8. r16 sn sn sn
  sn4\f sn8. sn16 \flam sn4 r
  sn16\< sn sn sn sn sn sn sn sn sn sn sn sn sn sn8\!
  %
  % part c
  sn16^> sn sn sn sn sn sn^> sn sn sn sn sn sn sn sn sn
  sn16 sn sn8 r16 sn sn sn sn sn sn8 r8. sn16
  sn16 sn sn8 r16 sn sn sn sn8. sn16 sn sn sn8
  r16 sn sn sn sn sn sn sn sn sn sn sn sn sn sn8
  %
  sn8 sn16 sn sn sn sn8 r16 sn8 sn16 sn sn sn8
  r16 sn sn sn sn sn8 sn16 sn sn sn8 r4
  sn16 sn sn8 r8 sn16 sn sn4 r16 sn sn sn
  sn8. sn16 sn sn sn8 r4 r16 sn16 sn8
  %
  sn16\mf sn sn sn sn sn sn sn sn8. sn16\< sn sn sn sn
  sn16 sn sn8\! sn16\> sn sn sn sn sn sn sn sn sn sn8\!
  sn16 sn sn8 sn16 sn sn8 r16 sn sn8 sn16 sn sn8
  r16 sn sn8 r16 sn sn sn sn sn sn sn sn sn sn sn
  %
  sn16 sn sn8 sn8 sn16 sn sn sn sn8 r8. sn16
  sn16 sn sn8 r8. sn16\mp sn sn8. r4
  r8. sn16\f sn8 r r2 \bar "|."

}
