% 
% Notes in parts a and c are grouped differently
% 
\include "definitions.ly"
\version "2.20.0"
\header {
  title = "2"
  meter = "Allegro moderato"
}
\drums {
  \time 2/4
  \tempo 2 = 120
  sn8[\f sn sn sn ]
  r8 sn sn sn
  sn8[ sn sn8 ] r
  r4 sn8 sn
  sn8[ sn sn8 ] r
  r4 sn8 sn
  sn8 sn4.
  sn8[ sn sn sn ]
  \break 

  % part a
  sn8[ r sn ] r
  sn8[ r sn ] r
  sn8[ sn sn ] r
  r4 sn8 sn
  sn8[ sn sn ] r
  r8 sn[ sn8 ] r
  r8 sn[ sn8 ] r
  r8 sn[ sn8 ] r
  \break
  sn8[ sn sn sn ]
  r8 sn[ sn sn ]
  sn8[ sn sn ] r
  r8 sn[ sn sn ]
  sn4 sn8 sn
  r8 sn[ sn sn ]
  sn4 ~ sn8 sn
  sn4 r
  \break

  % part b
  sn8\mf sn sn4
  sn8 sn sn4
  r8 sn sn4
  sn8 sn4 sn8
  sn8 sn sn4
  r8 sn sn sn
  sn8 sn sn sn
  r8 sn sn sn
  \break
  sn4\> sn8 sn
  sn4 sn8 sn
  sn4 sn8 sn
  sn4 sn8 sn
  sn4\p sn8 sn
  sn4 ~ sn8 sn
  sn8 sn sn4 ~
  sn8 sn sn sn
  \break
  sn4 r
  r2
  r8 sn sn sn
  sn4 ~ sn8 sn
  sn8 sn4.
  r4 sn
  sn8 sn sn sn
  r2
  \break
  sn8 sn4 sn8
  sn8 sn4 sn8
  sn8 sn sn4
  r8 sn sn sn
  sn8 sn4 sn8
  r4 sn
  sn8 sn sn sn
  r2
  \break

  % part c
  \drag sn8[\f r sn ] r
  r8 sn[ sn ] r
  r8 sn[ sn ] r
  sn8[ r sn ] r
  sn8[ sn sn sn ]
  r8 sn[ r sn ]
  r8 sn[ sn sn ]
  sn8[ sn sn ] r
  \break
  sn8[^> sn sn sn^> ]
  r8 sn[ sn sn ]
  sn8[ r sn ] r
  sn8[ sn sn ] r
  r4 sn8 sn
  sn8 r r4
  \break
  sn8[ sn sn8 ] r
  r4 sn
  sn8[ sn sn8 ] r
  r4 sn8\ff sn
  sn8 r r4 \bar "|."
  
}
