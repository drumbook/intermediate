% I injured my ankle and limped for a few days. This piece is based on
% the experience, but it turned out to be a nice roll exercise
% nonetheless.

\include "definitions.ly"
\version "2.20.0"
\header {
  title = "5"
  meter = "Moderato"
}
\drums {
  \time 2/4
  \tempo 4 = 112
  %
  % intro
  \partial 4 r8. sn16^>\f sn4:32 ~ sn8. sn16^>
  sn4:32 ~ sn8.^>  sn16
  sn4:32 ~ sn8. sn16^>
  sn4:32 ~ sn8.^>  sn16
  %
  \tuplet 3/2 { sn8 sn sn } sn4:32 ~
  sn8.^> sn16 sn4:32 ~
  sn8. sn16^> sn4:32 ~
  sn16^> sn8:32 ~ sn16 sn16^> sn8:32 ~ sn16
  sn16 sn sn8 r8. sn16
  %
  % part a
  sn4:32^> ~  sn8. sn16
  sn4:32^> ~  sn8. sn16
  sn16^> sn8:32 ~ sn16 sn16^> sn8:32 ~ sn16
  sn4^> sn8. sn16
  %
  sn4:32 ~ sn8. sn16^>
  sn4:32 ~ sn8.^> sn16
  sn4:32 ~ sn8. sn16^>
  sn4:32 ~ sn8.^> sn16
  %
  sn4:32 ~ sn8. sn16^>
  \time 3/4
  sn4:32 ~ sn8.^> sn16 \tuplet 3/2 { sn8 sn sn }
  sn2:32 ~ sn8. sn16^>
  \time 2/4
  sn4:32 ~ sn8.^> sn16
  \time 3/4
  sn2:32 ~ sn8. sn16^>
  sn4:32 ~ sn sn:32 ~
  %
  % part b
  \time 2/4
  sn8. sn16^> sn4:32 ~
  sn8. sn16^> sn4:32 ~
  sn8.^> sn16 sn4:32 ~
  sn8. sn16^> sn4:32 ~
  sn4 r8. sn16^>
  %
  sn4:32 ~ sn8. sn16^>
  sn4:32 ~ sn8 sn:32 ~
  sn4 sn:32^> ~
  sn8. sn16^> sn4:32 ~
  sn4 sn:32^> ~
  %
  \time 3/4
  sn8. sn16^> sn2:32 ~
  \time 2/4
  sn4 r
  %
  % part c
  \time 3/4
  \drag sn4\p r \drag sn\f
  sn4:32 ~ sn8. sn16^> sn4:32 ~
  sn4 r sn:32 ~
  sn8. sn16^> sn4:32 ~ sn
  %
  \drag sn4 r sn:32 ~
  sn8. sn16^> sn8:32 ~ sn sn4:32 ~
  sn8. sn16^> sn2:32 ~
  %
  \time 2/4
  sn4 r8. \flam sn16
  sn4:32 ~ sn8. \flam sn16
  sn4:32 ~ sn
  sn4:32 ~ sn8. \flam sn16
  sn4:32 ~ sn
  %
  sn4:32 ~ sn16 sn8.:32 ~
  sn4 sn:32 ~
  sn8. sn16^> sn4: 32 ~
  sn8.^>  sn16 sn4: 32 ~
  sn4 \tuplet 3/2 { sn8 sn sn }
  %
  % end
  sn4:32 ~ sn8. sn16^>
  sn4:32 ~ sn8. \flam sn16
  sn4:32 ~ sn8. \flam sn16
  r4 sn:32 ~
  sn8. sn16^> sn4:32 ~
  sn16^> sn8:32 ~ sn16 sn16^> sn8:32 ~ sn16
  \tuplet 3/2 { sn8\< sn sn } \tuplet 3/2 { sn sn sn }
  sn4^>\! r \bar "|."

}
