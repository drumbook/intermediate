# Intermediate studies for snare drum

This is an open source [snare drum
book](https://leanpub.com/drumbook-2) written in plain text and built
using free software.

## Dependencies

lilypond, latex, and make.

## Usage

Run `make` to build the PDF.

