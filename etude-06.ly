\include "definitions.ly"
\version "2.20.0"
\header {
  title = "6"
  meter = "Allegro"
}
\drums {
  \time 4/4
  \tempo 2 = 126
  % 
  % part a
  %
  r16 \drag sn\f sn sn sn4 \drag sn16 sn sn8 r \flam sn
  sn4:32 ~ sn16 sn8.:32 ~ sn16 sn sn sn sn8 sn
  \flam sn4 r16 sn8.:32\p ~ sn16 sn sn8 r16 sn sn sn
  sn16 sn sn8 sn sn sn:32 ~ sn r16 sn8.:32 ~
  sn8. sn16 sn8 sn \tuplet 3/2 { sn\< sn sn } sn\! sn:32 ~
  % 
  sn8^>\mf sn16 sn sn8 sn sn8^> sn16 sn sn8 sn
  sn16 sn^> sn sn sn8 sn sn16 sn sn sn sn sn sn sn
  sn8^> sn16 sn sn8 sn sn8^> sn16 sn sn8 sn
  sn16 sn^> sn sn sn8 sn \drag sn4^> r
  %
  sn16^>\f sn sn sn sn8 sn sn16^> sn sn sn sn8 sn
  sn8. sn16 sn sn sn sn sn sn sn8 r16 sn:32 sn sn
  sn8. sn16:32 sn sn sn8 r16 sn sn sn sn sn sn8
  sn16 sn^> sn sn sn8 sn sn4:32~ sn^>
  %
  % part b
  % 
  \time 2/4
  sn8\p sn:32 ~ sn8 sn:32 ~
  sn8 sn:32 ~ sn8 sn:32 ~
  sn8 \flam sn r \flam sn
  sn4.:32 ~ sn8
  % 
  r16 sn sn sn sn sn sn8:32 ~
  sn8 sn:32 ~ sn8 sn:32 ~
  sn16 sn^> sn sn sn8 \flam sn
  r8 \flam sn sn4:32 ~
  %
  sn8 sn:32 ~ sn8 sn:32 ~
  sn8 sn:32 ~ sn8 sn:32 ~
  sn16 sn^> sn sn sn8 sn
  sn4:32 ~ sn4^>
  %
  % part c
  % 
  \time 4/4
  sn8\mf sn16 sn sn sn sn8 r16 sn sn8 sn4:32 ~
  sn8. sn16 sn sn sn8 r16 sn sn8 sn4:32 ~
  sn8. sn16 sn8. sn16 sn8 sn sn \flam sn
  sn4:32 ~ sn16 sn^> sn sn sn8 sn \flam sn4
  %
  sn4:32 ~ sn16 sn sn sn sn4:32 ~ sn16 sn sn sn
  sn16^> sn8:32 ~ sn16 sn16^> sn8:32 ~ sn16 sn4^> sn16 sn sn sn
  sn4:32 ~ sn8 sn r16 sn8.:32 ~ sn8 \flam sn
  sn4:32 ~ sn16 sn^> sn sn sn8 sn \drag sn4
  % 
  sn16:32^> sn sn sn sn sn sn:32^> sn sn\< sn sn sn sn sn sn8\!
  sn16\f sn sn sn sn8 sn sn16 sn sn sn \tuplet 3/2 { sn8 sn sn }
  sn16:32\> sn8 sn16:32 sn8 sn16:32 sn r sn:32 sn8 sn4:32 ~
  sn8\!\< sn sn16 sn sn sn sn sn sn sn sn sn sn8\!
  %
  sn16 sn sn sn sn4:32 ~ sn16 sn sn8 r16 sn8.:32 ~
  sn16 sn sn sn sn4:32 ~ sn16 sn^> sn sn sn sn8.:32 ~
  sn8 r \drag sn16 sn sn8 r4 sn4:32\dim\fermata ~ sn1\!
  % sn4 sn2:32\dim\fermata ~ sn4\!
  \bar "|."

}
